# AWS Public Cloud - Firewall

Die vorliegenden Anleitung ist allgemein gestaltet, so dass verschiedene Module darauf verweisen können. Sie erstellen hier einen virtuellen Server in der Cloud mit dem Vorteil, dass Sie keine lokalen Ressource verwenden müssen. Diese Anleitung ist nicht mehr relevant, sobald die Lernenden Modul 346 hinter sich haben.

## Firewall einrichten

Markieren Sie Instanz für die Sie die Firewall, resp. Security Group einrichten möchten und wechseln Sie dann unten auf den Tab ***Security***. Klicken Sie dann auf den Link unten in Inhalt.

![firewall_1](./x_gitressources/firewall_1.png)



Sie haben nun die Sicherheitsgruppe (Security Group) geöffnet und können dort Ports freigeben. Stellen Sie sicher, dass Sie sich bei ***Inbound rules*** befinden. Klicken Sie dann auf den Knopf ***Edit inbound rules***

![firewall_2](./x_gitressources/firewall_2.png)



Geben Sie hier nun die Ports frei, die Ihnen die Lehrperson nennt. Verwenden Sie einfach die Einstellungen wie unten dargestellt, aber passen Sie den Port jeweils an. Sie können hier mehrere Ports nacheinander freigeben.

![firewall_3](./x_gitressources/firewall_3.png)

Typische Ports:

- SSH: 22
- HTTP(s): / 80, 443

- (S)FTP(s): 21
- MySql: 3306
