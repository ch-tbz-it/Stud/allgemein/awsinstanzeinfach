# AWS Public Cloud - Web Konsole

Die vorliegenden Anleitung ist allgemein gestaltet, so dass verschiedene Module darauf verweisen können. Sie erstellen hier einen virtuellen Server in der Cloud mit dem Vorteil, dass Sie keine lokalen Ressource verwenden müssen. Diese Anleitung ist nicht mehr relevant, sobald die Lernenden Modul 346 hinter sich haben.

[TOC]

## Verbindung aufbauen

Sie können sich mit einer Web-Konsole Ihrer Instanz verbinden - direkt in AWS.

Das folgende Bild zeigt Ihnen die Schritte.

![access_awsweb](/x_gitressources/access_awsweb.png)

Auf dem nachfolgenden Schritt (ohne Screenshot hier), können Sie die Standard-Einstellungen belassen und einfach auf den orangen Knopf unten rechts klicken.
