# AWS Public Cloud - Public IP

Die vorliegenden Anleitung ist allgemein gestaltet, so dass verschiedene Module darauf verweisen können. Sie fügen einem virtuellen Server eine öffentliche IP hinzu, so dass Sie im gesamten Modul die gleiche IP verwenden können. Diese Anleitung ist nicht mehr relevant, sobald die Lernenden Modul 346 hinter sich haben.

## IP erstellen und zuweisen

Wir weisen eine öffentliche IP zu, damit Ihre Instanz immer die gleiche IP behalten kann.

Sie müssen auf der linken Seite herunter-scrollen und den Navigationspunkt *Elastic IPs* auswählen. Weisen Sie dann eine IP-Adresse zu mit dem Orangen Knopf. Im geöffneten Fenster können Sie die Standardeinstellungen sein lassen. 



![ip_1](x_gitressources/ip_1.png)



Die erstellte IP müssen Sie nun Ihrer Instanz zuweisen.

![ip_2](x_gitressources/ip_2.png)

Wenn Sie in das *Instance*-Feld klicken, können Sie nach der Instanz suchen. Das Feld *Private IP-Adresse* zeigt Ihnen die bisherige private IP an (wenn Sie reinklicken). Verwenden Sie einfach diese wieder. Sie wird vom Wert unten im Screenshot abweichen.

![ip_3](x_gitressources/ip_3.png)



