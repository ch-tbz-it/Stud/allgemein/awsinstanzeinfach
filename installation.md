# AWS Public Cloud - Installation

Die vorliegenden Anleitung ist allgemein gestaltet, so dass verschiedene Module darauf verweisen können. Sie erstellen hier einen virtuellen Server in der Cloud mit dem Vorteil, dass Sie keine lokalen Ressource verwenden müssen. Diese Anleitung ist nicht mehr relevant, sobald die Lernenden Modul 346 hinter sich haben.

## Installation

Die Lehrperson hat Sie bereits zu einem Amazon/AWS Kurs eingeladen. Sie bekommen die Einladung via E-Mail.

Login-Seite: [Log In to Canvas (instructure.com)](https://awsacademy.instructure.com/login/canvas)

Wählen Sie nun den Kurs "**Lerner Lab**" aus:

![steps_01](./x_gitressources/steps_01.png)


Auf der nächsten Seite navigieren Sie durch "**Modules**", dann "**Lerner Lab**".

![steps_02](./x_gitressources/steps_02.png)


Klicken Sie nun auf "**Start Lab**". Beim ersten Mal dauert dies ein paar Minuten, weil der Account provisioniert werden muss. sobald der grün Punkte links grün ist, klicken Sie auf den **AWS**-Link (neben dem grünen Punkt).

![steps_03](./x_gitressources/steps_03.png)


Scrollen Sie runter bis Sie die Links im nächsten Screenshot sehen. Starten Sie eine virtuelle Maschine.

![steps_04](./x_gitressources/steps_04.png)

Es gibt viele Option zur Konfiguration einer VM. Setzen Sie die Optionen **wie sie Ihnen von der Lehrperson vorgegeben** wird. Normalerweise reichen die folgenden Optionen hier beispielhaft für ein Modul m122:

- Name: Verwenden Sie einen sprechenden Name (z.B. "m122" oder "m293 FTP/Web", etc)
- Betriebssystem: Üblicherweise Ubuntu in der neusten Version
- Instanz-Typ. t1-Mikro: Nehmen Sie keine grössere Instanz, da Ihre Ressourcen in dieser Schulungsumgebung limitiert sind - ausser die Lehrperson nennt eine andere
- Schlüsselpaar: Verwenden Sie den bestehenden Eintrag "vockey"
- Speichergrösse: 20GB
- **Wichtig**: Zusätzliche Einstellung: Die Lehrperson wird Ihnen meistens ein Konfigurationsskript geben. Kopieren Sie dessen Inhalt in "User data" unter "Advanced" (in das Textfeld)

Folgend die Bilder dazu:

**Instanzname**

![steps_05](./x_gitressources/steps_05.png)



**Betriebssystem**

![steps_06](./x_gitressources/steps_06.png)



**Instanz-Typ**

![steps_07](./x_gitressources/steps_07.png)

**Schlüsselpaar**

![steps_08](./x_gitressources/steps_08.png)



**Speichergrösse**

![steps_09](./x_gitressources/steps_09.png)



**User data** innerhalb "**Advanced  details**" (ganz unten)

![steps_10](./x_gitressources/steps_10.png)

Nun können sie die Instanz über den **orangen** Knopf in der rechten Spalte starten. Nach dem Starten können Sie auf die Liste der Instanzen gehen und sollten dort die neue Instanz sehen.

![steps_11](./x_gitressources/steps_11.png)
