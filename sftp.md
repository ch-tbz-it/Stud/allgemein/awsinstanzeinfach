# AWS Public Cloud - SSH

Die vorliegenden Anleitung ist allgemein gestaltet, so dass verschiedene Module darauf verweisen können. Sie erstellen hier einen virtuellen Server in der Cloud mit dem Vorteil, dass Sie keine lokalen Ressource verwenden müssen. Diese Anleitung ist nicht mehr relevant, sobald die Lernenden Modul 346 hinter sich haben.



## Zugang mit WinScp

Falls Sie Dateien manuell auf Instanzen hochladen möchten, können Sie dies via [WinScp](https://winscp.net/eng/download.php) tun. 

Erstellen Sie eine neue "site" und geben Sie die IP ihrer virtuellen Instanz ein. Verwenden Sie "ubuntu" as Benutzername. Verwenden Sie das korrekte Passwort oder Private Key Datei.

![access_04](./x_gitressources/access_03.png)



Ändern Sie dann die SFTP server settings unter "**advanced**". Dies erlaubt Ihnen den Zugriff als "sudo" also mit vollen Admin-Rechten.

![access_05](./x_gitressources/access_05.png)
