# AWS Instanz Erstellen

Diese Anleitung dient als Basis für verschiedene Module. Die Details werden pro Modul von der Lehrperson  übergeben. 

Diese Anleitung ist nur solange relevant bis die Lernenden das Modul 346 durchgearbeitet haben. Anschliessend sollte eine Instanz eigenständig erstellt werden können.



## Anleitungen

[Virtuelle Maschine in AWS Cloud erstellen](./installation.md)

[Firewall einrichten](./firewall.md)

[Öffentliche IP hinzufügen](./publicIP.md)



Auf die virtuellen Server zugreifen:

[Zugriff via AWS Web Konsole](./awsweb.md)

[Zugriff via SSH/Konsole](./ssh.md)

[Zugriff via SFTP](./sftp.md)
