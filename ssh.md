# AWS Public Cloud - SSH

Die vorliegenden Anleitung ist allgemein gestaltet, so dass verschiedene Module darauf verweisen können. Sie erstellen hier einen virtuellen Server in der Cloud mit dem Vorteil, dass Sie keine lokalen Ressource verwenden müssen. Diese Anleitung ist nicht mehr relevant, sobald die Lernenden Modul 346 hinter sich haben.

[TOC]

## IP der Instanz finden

Gehen Sie nun zu Ihren Instanzen und wählen Sie ihre erstelle Instanz aus. In den Details können Sie nun die IP kopieren.

**ACHTUNG**: Diese IP wechselt jedes mal, wenn Sie die Instanz neu starten - ausser Sie haben vorher eine öffentliche IP explizit zugewiesen.

![access_01](./x_gitressources/access_01.png)

Mit der IP können Sie nun auf Ihre virtuelle Instanz zugreifen mit oder ohne Passwort, abhängig von der vorgegebenen Konfiguration. Ihre Lehrperson muss Ihnen die Variante vorgeben.



## SSH mit Passwort

~~~bash
ssh ubuntu@54.145.177.144 -o ServerAliveInterval=30
~~~

Im vorgegebenen Befehl müssen Sie die folgenden Parameter ändern:

- Benutzer. Per Default ist der Benutzer "ubuntu" auf Ubuntu Instanzen
- IP. Wie Sie die korrekte IP kriegen, haben Sie bereits gesehen.

Nun müssen Sie den **fingerprint aktzeptieren** indem Sie "yes" schreiben. Dies machen Sie jedesmal, wenn die IP ändert. 

**Das Passwort wird ihnen von der Lehrperson mitgeteilt.**

Nach erfolgreichem Einloggen, sollten Sie ein Bild wie das folgende vorfinden.

![access_02](./x_gitressources/access_02.png)



## SSH mit Private Key

~~~bash
ssh ubuntu@54.145.177.144 -i <pfad-zu-datei> -o ServerAliveInterval=30
# Beispiele:
# ssh ubuntu@54.145.177.144 -i C:\Users\Muster\.ssh\keyfile.pem -o ServerAliveInterval=30
# ssh ubuntu@54.145.177.144 -i "C:\Users\Muster Max\.ssh\keyfile.pem" -o ServerAliveInterval=30
~~~

Im vorgegebenen Befehl müssen Sie die folgenden Parameter ändern:

- Benutzer. Per Default ist der Benutzer "ubuntu" auf Ubuntu Instanzen
- IP. Wie Sie die korrekte IP kriegen, haben Sie bereits gesehen.
- Den Pfad auf die Private-Key-Datei. Wie Sie diese Datei bekommen, zeigt Ihnen die Lehrperson

Nun müssen Sie den **fingerprint akzeptieren** indem Sie "yes" schreiben. Dies machen Sie jedes mal, wenn die IP ändert. 

Nach erfolgreichem Einloggen, sollten Sie ein Bild wie das folgende vorfinden.

![access_02](./x_gitressources/access_02.png)

